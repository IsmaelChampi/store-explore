<?php

 ?>

 <!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="UTF-8" />
    <title>Exploore</title>
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700&display=swap" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/fontawesome/css/all.min.css" rel="stylesheet" />
    <link rel="icon" type="image/png" href="images/favicon.png" />
    <link href="https://fonts.googleapis.com/css?family=Fjalla+One|Source+Sans+Pro" rel="stylesheet">
     <link rel="stylesheet" href="css/media.css" media="screen and (max-width:768px)"/>

    <style>
    h1{font-family:'Raleway',sans-serif;font-size:150%;padding:10px}p{font-family:'Raleway',sans-serif;font-size:100%;font-size:17px}li{font-family:'Raleway',sans-serif;font-size:100%;font-size:17px}.tab-content{font-family:'Raleway',sans-serif;font-size:100%;font-size:17px}.header{background-color:#283747;color:white;display:flex;width:100%;align-items:center;justify-content:space-between;flex-wrap:wrap}.iconnav{width:100%;height:auto;display:flex;background-color:#283747;border-bottom:solid 1px #ffffffbf}.fa-envelope{margin-left:20%}.face1{margin-right:10%;color:white}.iconrigth{background-color:;width:50%;height:auto;padding:10px 0px;display:inline-block}i.fas.fa-phone-alt,.fa-envelope{font-size:18px;margin-left:10px}.iconrigth span{margin-left:10px}.iconleft{width:50%;height:auto;padding:10px 0px;display:flex}.container{display:flex;width:100%}.iconlogo{width:50%;background-color:}.menu li{display:inline-block;margin-left:10px;color:white}.iconmenu{width:50%;background-color:}.menu{float:right;margin-right:30px}.white{color:white}.div1{background-color:#d6dcde00;display:block;width:100%;margin-top:25%}.iconos1{width:50%;height:auto}.iconos2{width:50%;height:auto}.img-hero{background-image:url('/img/banner1.png');background-color:;background-size:100%;background-repeat:no-repeat}.flexbox{display:flex;flex-wrap:wrap;align-items:center;justify-content:center}.box{flex-shrink:0;margin:0px;width:100%;height:569px;background:#00000078}.btn1{background-color:#e0e02aa6;width:17%;height:9%;border-radius:6px;display:block;padding:10px;background:rgb(241,225,95);background:linear-gradient(180deg,rgba(241,225,95,1) 0%,rgba(242,201,47,1) 100%);border:none}.flexbox2{display:flex;flex-wrap:wrap;align-items:center;justify-content:center}.box2{flex-shrink:0;margin:0px;width:100%;height:235px;background-color:#F7DC6F}.form{width:100%;height:100%;background-color:}.btnform2{background-color:#000000a6;width:53%;height:33px;border-radius:4px}.formcenter{width:80%;height:100%;background-color:;margin-left:10%;display:flex}.form-box{width:20%;height:auto;position:relative;display:block}.form-two{display:flex}.form-50{width:50%;padding:0px 10px}.form-box button{margin-top:25%}input.form-control{width:95%}.tab-content{display:none;background:;width:50%;height:50%}@media screen and (max-width:795px){.iconmenu{width:81%;background-color:}.box{flex-shrink:0;margin:0px;width:100%;height:420px;background:#00000078}.btn1{background-color:#e0e02aa6;width:18%;height:9%;font-size:16px;border-radius:6px;display:block;padding:10px;background:rgb(241,225,95);background:linear-gradient(180deg,rgba(241,225,95,1) 0%,rgba(242,201,47,1) 100%);border:none}.form-box{width:20%;height:auto;margin:6px;position:relative;display:block}.formcenter{width:auto;height:100%;background-color:;margin-left:10%;display:flex}.formcenter{width:auto;height:100%;background-color:;margin-left:0%;display:flex}.form-box{width:20%;height:auto;margin:6px;position:relative;display:block}}@media screen and (max-width:480px){.div1{background-color:#d6dcde00;display:block;width:50%;margin:0 auto}.iconrigth{background-color:;width:100%;height:auto;padding:10px 0px;display:inline-block}.iconleft{width:100%;height:auto;padding:10px 0px;display:flex}.iconos1{width:50%;height:auto}.iconos2{width:50%;height:auto}.img-hero{background-image:url('/img/banner1.png');background-color:;background-size:cover;background-repeat:no-repeat}.flexbox2{display:block;align-items:center;justify-content:center}.box2{flex-shrink:0;margin:0px;width:100%;height:auto;background-color:#F7DC6F}.formcenter{width:auto;height:100%;background-color:;margin-left:5%;display:block}.form-box{width:100%;height:auto;margin:-1px;position:relative;display:block}.tab-content{display:none;background:;width:100%}.btnform2{background-color:#000000a6;width:53%;height:33px;border-radius:4px;font-size:18px}.form-box{width:100%;height:auto;margin:-1px;position:relative;display:block;padding:4px}.form-box button{margin-top:15%}}}
    </style>
    <meta name="viewport" content="width=device-width, initial-scale=1"> 

    <script
  src="https://code.jquery.com/jquery-2.2.4.min.js"
  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
  crossorigin="anonymous"></script>


  <?php wp_head(); ?>

  </head>
  <body>
   <header class="header">
    <div class="iconnav">
      <div class="iconrigth">
        <i class="fas fa-phone-alt"><span>+123 2226947188</span></i>
        <i class="fas fa-envelope"><span>ismael@gmail.com</span></i>
      </div>
      <div class="iconleft">
        <div class="iconos1">
        <i class="fab face1 fa-facebook-f"></i>
        <i class="fab face1 fa-google-plus-g"></i>
        <i class="fab face1 fa-youtube-square"></i>
        <i class="fab face1 fa-twitter"></i>
        </div>
        <div class="iconos2">
        <a href=""><i class="fas  face1 fa-user">Login</i></a>
        <a href=""><i class="fas face1 fa-key">Sign Up</i></a>
        </div>
      </div>
    </div>
      <div class="container">
              <div class="iconlogo">
        <figure class="logo">
          <img src="<?php echo get_template_directory_uri(); ?>/img/logo.png"/>
        </figure>
      </div>
      <!-- <div class="iconmenu"> -->
        <!-- <nav class="menu"> -->

           <?php wp_nav_menu( array( 'header-menu' => 'header-menu' ) ); ?>
         <!--  <ol>
            <li>
              <a class="link white" href="#portafolio">Home</a>
            </li>
            <li>
              <a class="link white" href="#eventos">Destination</a>
            </li>
            <li>
              <a class="link white" href="#contacto">Discount</a>
            </li>
             <li>
              <a class="link white" href="#contacto">About</a>
            </li>
             <li>
              <a class="link white" href="#contacto">Blog</a>
            </li>
             <li>
              <a class="link white" href="#contacto">Contact</a>
            </li>
          </ol> -->
       <!--  </nav> -->
       <!--  </div> -->
      </div> </header>
 

