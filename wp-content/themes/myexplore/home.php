<?php
/**
* Template Name: Home
 */

get_header(); ?>

<section class="hero img-hero">
     <div class="flexbox">
    <div class="box">
      <div class="div1">
      <div class="hover">
      <center><h1 class="h1" style="color:#FDFEFE">
        <strong>Start Your Greatest</strong><br>ADVENTURE WITH US!</h1></center>
      <center><P style="color:#FDFEFE";>Lorem ipsum dolor sit amet,consectetur adipiscing elit, sed do eiusmod tempor incididunt<br> ut labore et dolore magna aliqua. Ut enim ad minim veniam.</P></center>
      <center><button type="button" class="btn1 btn-outline-warning">CLICK HERE</button></center>
      </div>
       </div>
    </div>
    </div>
    </section>
    <section id="">
    <div class="flexbox2">
    <div class="box2">
      <div class="form">
        <div class="formcenter">
          <div class="form-box">
            <center><h1>Where?</h1></center>
            <center><input type="password" class="form-control"  placeholder="Location"></center>
          </div>
           <div class="form-box">
            <center><h1>Check In</h1></center>
            <center><input type="password" class="form-control"  placeholder="DD-MM-YYYY"></center>
          </div>
           <div class="form-box">
            <center><h1>Check Out</h1></center>
            <center><input type="password" class="form-control"  placeholder="DD-MM-YYYY"></center>
          </div>
           <div class="form-box">

<div class="form-two">

 <div class="form-50">  <center><h1>Adult</h1></center>
            <center><input type="password" class="form-control"  placeholder="01"></center></div>

          
            <div class="form-50">  <center><h1>Child</h1></center>
            <center><input type="password" class="form-control"  placeholder="00"></center></div>
 </div>

          </div>
           <div class="form-box">
            <center><button style="color:#FDFEFE" type="button" class="btnform2 btn-outline-warning">CLICK HERE</button></center>
          </div>
        </div>
  </div>
    </div>
  </div>
    </section>
    <section id="">
       <div class="flexbox3">
    <div class="box3">
      <img src="<?php echo get_template_directory_uri(); ?>/img/image1.png" class="imgbox3">
    </div>
    <div class="box3 img3">
      <div class="cag3">
      <h1 class="cag3">Trusted Since 2001</h1><h4 class="cag3">We are Explore</h4>
      <p class="cag3">Lorem ipsum dolor sit amet, consectectur adipiscing elit, sed do<br>eiusmod tempor incididunt ut labore et dolore magna aliqua.ut<br>enim ad minim veniam, quis nostrud exercitation ullamco laboris.</p>
      <ul>
          <li class="lista3"><i class="fas fa-check-circle"></i>Lorem ipsum dolor sit amet</li>
          <br>
          <li class="lista3"> <i class="fas fa-check-circle"></i>Lorem ipsum dolor sit amet</li>
          <br>
          <li class="lista3"> <i class="fas fa-check-circle"></i>Lorem ipsum dolor sit amet</li>
      </ul>
    </div>
    </div>
  </div>
    </section>
    <section id="">
         <div class="flexbox4">
          <center><h1>Our Best Services</h1></center>
          <center><p>We are Travel Agent</p></center>
    <div class="box4">
      <div class="mod4">
        <div class="mod6">
        <div class="cua1">
          <i class="fas icon4 fa-plane"></i>
          <p>Lorem ipsum dolor sit</p>
        </div>
        <div class="cua1">
          <i class="fas icon4 fa-hotel"></i>
          <p>Lorem ipsum dolor sit</p>
        </div>
       <div class="cua1">
         <i class="fas icon4 fa-utensils"></i>
          <p>Lorem ipsum dolor sit</p>
        </div>
        </div>
    </div>
      <div class="mod4-1">
        <div class="mod6 mod8">
        <div class="cua1">
          <i class="fas icon4 fa-train"></i>
          <p>Lorem ipsum dolor sit</p>
          </div>
        <div class="cua1">
          <i class="fas icon4 fa-train"></i>
          <p>Lorem ipsum dolor sit</p>
        </div>
        <div class="cua1">
          <i class="far icon4 fa-eye"></i>
          <p>Lorem ipsum dolor sit</p>
        </div>
        </div>
    </div>
  </div>
  </div>
    </section>
     <section id="">
         <div class="flexbox5 img5">
    <div class="box5">
      <center><h1>Trusted Since 2001<br><p>We are Travel Agent</p></h1></center> 
      <div class="cajag">
        <div class="caja5">
         <center><i class="fas fa-car"></i></center>
          <center><h1>Low Budget Trip</h1></center>
          <div class="yellow1">
            <strong><center>$500</center></strong>
          </div>
          <div class="textoyellow">
              <ul>
          <li class="lista3"><i class="fas fa-check-circle"></i>Lorem ipsum dolor</li>
          <br>
          <li class="lista3"> <i class="fas fa-check-circle"></i>Lorem ipsum dolor</li>
          <br>
          <li class="lista3"> <i class="fas fa-check-circle"></i>Lorem ipsum dolor</li>
      </ul>
          </div>
        </div>
         <div class="caja5">
         <center><i class="fas fa-train"></i></center>
          <center><h1>Low Budget Trip</h1></center>
          <div class="yellow1">
            <strong><center>$500</center></strong>
          </div>
          <div class="textoyellow">
              <ul>
          <li class="lista3"><i class="fas fa-check-circle"></i>Lorem ipsum dolor</li>
          <br>
          <li class="lista3"> <i class="fas fa-check-circle"></i>Lorem ipsum dolor</li>
          <br>
          <li class="lista3"> <i class="fas fa-check-circle"></i>Lorem ipsum dolor</li>
      </ul>
          </div>
        </div>
        <div class="caja5">
         <center><i class="fas fa-plane"></i></i></center>
          <center><h1>Low Budget Trip</h1></center>
          <div class="yellow1">
            <strong><center>$500</center></strong>
          </div>
          <div class="textoyellow">
              <ul>
          <li class="lista3"><i class="fas fa-check-circle"></i>Lorem ipsum dolor</li>
          <br>
          <li class="lista3"> <i class="fas fa-check-circle"></i>Lorem ipsum dolor</li>
          <br>
          <li class="lista3"> <i class="fas fa-check-circle"></i>Lorem ipsum dolor</li>
      </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
    </section>
     <section id="">
       <div class="flexbox6">
    <div class="box6">
      <center><img src="<?php echo get_template_directory_uri(); ?>/img/image3.png" class="im6"></center>
    </div>
    <div class="box6-1">
      <div class="cuadro">
        <h1>Trusted Since 2001<br><p>We are Travel Agent</p></h1>
      <div class="tabcontainer">
        <ul class="menu-slide tabs">
    <li class="azul tab-link current" data-tab="tab-1" ><i class="fas fa-plane"></i> <span>Flight</span></li>
    <li class="gris tab-link" data-tab="tab-2"><i class="fas fa-car"></i><span>Cab</span></li>
    <li class="blanco tab-link" data-tab="tab-3"><i class="fas fa-hotel"></i><span>Hotel</span></li>
      </ul> 
  <br>
      <br>
    

  <div id="tab-1" class="tab-content current">
   By default, the text template uses the default global cover page, which is set up in the fax server configuration, cover page tab.
  </div>
  <div id="tab-2" class="tab-content">
    Alternatively you can click the necessary text effect with the right mouse button and select Add or Replace Text option of the express menu. 
  </div>
  <div id="tab-3" class="tab-content">
  Luckily there is a pyconstraint template bundled with Blender and available in the Text Editor where 50% of the job is done for us. 
  </div>


</div><!-- container -->
<br>
      <div class="buttoncontainer">
      <button type="button" class="btn btny btn-outline-warning">CLICK HERE</button>
</div>
      </div>
    </div>
  </div>
    </section>
       <section id="">
         <div class="flexbox7 img7">
    <div class="box7">
      <center><h1>Pack and Go</h1></center>
      <div class="cajag7">
        <div class="caja7">
          <img src="<?php echo get_template_directory_uri(); ?>/img/image6.png" class="imgca7">
         <center><div class="texto7">
              <p>7 Day + 6 Night</p>
              <h1>New York + Paris</h1>
              <p>lorem ipsum dolor</p>
              <p>lorem ipsum dolor</p>
              <h1>$600</h1>
              <button type="button" class="btnseccion7 btn-outline-warning">CLICK HERE</button>
          </div></center>
         
        </div>
         <div class="caja7-1">
          <img src="<?php echo get_template_directory_uri(); ?>/img/image5.png" class="imgca7">
          <center><div class="texto7">
             <p>7 Day + 6 Night</p>
              <h1>New York + Paris</h1>
              <p>lorem ipsum dolor</p>
              <p>lorem ipsum dolor</p>
              <h1>$600</h1>
              <button type="button" class="btnseccion7 btn-outline-warning">CLICK HERE</button>
          </div></center>
        </div>
         <div class="caja7-1-1">
          <img src="<?php echo get_template_directory_uri(); ?>/img/image7.png" class="imgca7">
          <center><div class="texto7">
              <p>7 Day + 6 Night</p>
              <h1>New York + Paris</h1>
              <p>lorem ipsum dolor</p>
              <p>lorem ipsum dolor</p>
              <h1>$600</h1>
              <button type="button" class="btnseccion7 btn-outline-warning">CLICK HERE</button>
          </div></center>
        </div>
        </div>
    </div>
  </div>
    </section>
    <!-- ajdgsgjhgd -->
      <section id="">
         <div class="flexbox8 img8">
    <div class="box8">
      <center><h1>Pack and Go</h1></center>
      <div class="cajag8">
        <div class="caja8">
          <img src="<?php echo get_template_directory_uri(); ?>/img/image5.png" class="imgca8">
         <center><div class="texto8">
              <button type="button" class="btnseccion8 btn-outline-warning">CLICK HERE</button>
              <center><h1>Lorem Ipsum Dolor</h1></center>
      <center><p>Lorem ipsum dolor sit amet<br>consetetur adispliscing eliot.<br>Lorem ipsum dolor sit amet.</p></center>

          </div></center>
         
        </div>
         <div class="caja8-1">
          <img src="<?php echo get_template_directory_uri(); ?>/img/image6.png" class="imgca8">
          <center><div class="texto8">
              <button type="button" class="btnseccion8 btn-outline-warning">CLICK HERE</button>
              <center><h1>Lorem Ipsum Dolor</h1></center>
      <center><p>Lorem ipsum dolor sit amet<br>consetetur adispliscing eliot.<br>Lorem ipsum dolor sit amet.</p></center>

          </div></center>
        </div>
         <div class="caja8-1-1">
          <img src="<?php echo get_template_directory_uri(); ?>/img/image7.png" class="imgca8">
          <center><div class="texto8">
              <button type="button" class="btnseccion8 btn-outline-warning">CLICK HERE</button>
              <center><h1>Lorem Ipsum Dolor</h1></center>
      <center><p>Lorem ipsum dolor sit amet<br>consetetur adispliscing eliot.<br>Lorem ipsum dolor sit amet.</p></center>

          </div></center>
        </div>
        </div>
    </div>
  </div>
    </section>
    <section id="">
       <div class="flexbox9">
    <div class="box9 img9">
       <div class="cajag9">
        <div class="caja9">
        </div>
        <div class="caja9-1">
        </div>
        <div class="caja9-1-1">
        </div>
      </div>
    </div>
    <div class="box9-1">
       <div class="mindiv">
        <h1>New York + Paris</h1><p>7 Day + 6 Night</p>
        <!-- iconos -->
        <p>Lorem ipsum dolor sit amet. consectetur adipiscing elit, sed do<br>eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
        <h1>$1000</h1>
        <button type="button" class="btnpalma btn-outline-warning">CLICK HERE</button>
      </div>
    </div>
  </div>
    </section>
      <section id="">
       <div class="flexbox10 img10">
    <div class="box10-1">
      <div class="div10">
      <center><h1 style="color:#FDFEFE">Customer Reviews<br><h2 style="color:#FDFEFE">We are Travel Agent</h2></h1></center>
      <center><p style="color:#FDFEFE">Lorem ipsum dolor sit amet,consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p></center>
      <br>
      <center><div class="star">
      <!-- estrellas -->
      <i class="fas fa-star"></i>
      <i class="fas fa-star"></i>
      <i class="fas fa-star"></i>
      <i class="fas fa-star"></i>
      <i class="fas fa-star"></i>
      </div></center>
      <br>
      <!-- John Smith -->
      <center><div class="jon">
      <!-- estrellas -->
      <img src="<?php echo get_template_directory_uri(); ?>/img/cara.png" class="isma"><p class="jon2">Ismael Hdz</p>
      </div></center>
    </div>
    </div>
     </div>
    </section>
      <section id="">
       <div class="flexbox12 img11">
    <div class="box12">
       <center><h1 style="color:#FDFEFE">Customer Reviews<br><h4 style="color:#FDFEFE">We are Travel Agent</h4></h1></center>
       <div class="addres1">
        <center><i class="fas iconadres fa-map-marker-alt"></i></center>
       </div>
    </div>
    </div>
    </section>

    home.css
<?php
get_footer();